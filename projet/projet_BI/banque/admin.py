from django.contrib import admin
from .models import Client, Compte, Transaction

admin.site.register(Client)
admin.site.register(Compte)
admin.site.register(Transaction)


