from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils import timezone


# Base de données client #
class Client(models.Model) :
    nom_client = models.CharField(max_length=100)
    prenom_client = models.CharField(max_length=100)
    username = models.CharField(max_length=100, default='none')
    mdp = models.CharField(max_length=100)
    date = models.DateField(auto_now=True)      # Pour avoir la date de création du compte
    iban = models.CharField(max_length=100, unique=True)
    livret_A = models.CharField(max_length=100, unique=True)

    def generer_iban(self):
        # Utiliser l'Id auto-incrémenté de l'instance Client pour générer l'IBAN
        self.iban = f"FR{self.id}"
    
    def generer_livretA(self):
        # Utiliser l'Id auto-incrémenté de l'instance Client pour générer le numéro du livret A
        self.livret_A = f"A{self.id}" 

    def __str__(self) :
        # Permet de remplacer le "Client object" comme ceci Prenom_Nom_id=numero dans l'admin                   
        return f"{self.prenom_client}_{self.nom_client}_id={self.id}"
    
    def get_nombre_comptes(self):
        return Compte.objects.filter(client=self).count()
    
    def creer_nouveau_compte(self, type_compte, solde):
        nb = self.get_nombre_comptes()
        if type_compte=='Livret A':
            number = f"A{self.id}{nb}"
        else:
            number = f"FR{self.id}{nb}"
        compte = Compte.objects.create(client=self, type_compte=type_compte, numero=number, solde=solde)
        return compte
    
class Compte(models.Model) : 
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    type_compte = models.CharField(max_length=100)
    numero = models.CharField(max_length=100)
    solde = models.IntegerField()
    
    def faire_un_depot(self, montant):
        self.solde += montant
        self.save()
        
    def faire_un_retrait(self, montant):
        if montant > 0 and montant <= self.solde:
            self.solde -= montant
            self.save()

    def __str__(self):
        return f"Compte #{self.id} - Client: {self.client}, Type: {self.type_compte}, Solde: {self.solde}"
    
    DEPOT = 'DEPOT'
    RETRAIT = 'RETRAIT'
    
    def creer_transaction(self, montant, type_transaction):
        transaction = Transaction.objects.create(compte=self, montant=montant, type_transaction=type_transaction, date_transaction=timezone.now())
        return transaction
            
class Transaction(models.Model):
    compte = models.ForeignKey('Compte', on_delete=models.CASCADE)
    type_transaction = models.CharField(max_length=100)
    montant = models.IntegerField()
    date_transaction = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Transaction #{self.id} - Compte: {self.compte}, Montant: {self.montant}, Type: {self.type_transaction}, Date: {self.date_transaction}"
        
        
    
    
