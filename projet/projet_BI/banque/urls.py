from django.urls import path, include
from . import views

""" urlpatterns = [
    path('client/<str:name>/', views.client, name='client'), 
] """

urlpatterns = [
    path('client/<int:client_id>/', views.client2, name='client2'),
    path('faire_depot/<int:numero>/', views.faire_depot, name='faire_depot'),
    path('faire_retrait/<int:numero>/', views.faire_retrait, name='faire_retrait')
]