from django.shortcuts import render, redirect
from .models import Client, Compte, Transaction

# Create your views here.

def banque_index(request) :
    return render(request, 'banque/banque_index.html')

def client2(request, client_id):
    client = Client.objects.get(pk=client_id)
    comptes_bancaires = Compte.objects.filter(client=client)
    nombre_comptes = client.get_nombre_comptes()

    transactions_par_compte = {}  # Dictionnaire pour stocker les transactions par compte

    for compte in comptes_bancaires:
        transactions_par_compte[compte] = Transaction.objects.filter(compte=compte)
        
    print(transactions_par_compte)

    if request.method == 'POST':
        # Vérifiez si le formulaire de création de compte est soumis
        if 'creer_compte' in request.POST:
            type_compte = request.POST.get('type_compte')
            solde_initial = int(request.POST.get('solde_initial', 0))

            # Assurez-vous que le solde initial est positif
            if solde_initial > 0:
                client.creer_nouveau_compte(type_compte, solde_initial)
                return redirect('client2', client_id=client.id)

    return render(request, 'banque/client.html', {'client': client, 'comptes_bancaires': comptes_bancaires, 'nombre_comptes': nombre_comptes, 'transactions_par_compte': transactions_par_compte})

def faire_depot(request, numero):
    if request.method == 'POST':
        compte = Compte.objects.get(pk=numero)
        montant_depot = int(request.POST.get('montant_depot', 0))

        if montant_depot > 0:
            compte.faire_un_depot(montant_depot)
            compte.creer_transaction(montant_depot, Compte.DEPOT)

    return redirect('client2', client_id=compte.client.id)

def faire_retrait(request, numero):
    if request.method == 'POST':
        compte = Compte.objects.get(pk=numero)
        montant_retrait = int(request.POST.get('montant_retrait', 0))

        if montant_retrait > 0:
            compte.faire_un_retrait(montant_retrait)
            compte.creer_transaction(montant_retrait, Compte.RETRAIT)

    return redirect('client2', client_id=compte.client.id)


def client(request, name) :                             # On passe le nom en paramètre pour pouvoir afficher les infos 
                                                        # liés au client
    return render(request, 'banque/client.html')