from django import forms
import re

# Form django pour le fichier connexion.html
class connexionForm(forms.Form):                  
    identifiant = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'placeholder': 'Identifiant'}))
    mot_de_passe = forms.CharField(max_length=100, widget=forms.PasswordInput(attrs={'placeholder': 'Mot de passe'}))   # widget = masquer le mot passe 


# Form django pour le fichier souscription.html
class souscriptionForm(forms.Form):                  
    nom = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'placeholder': 'Nom'}))
    prenom = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'placeholder': 'Prénom'}))
    mot_de_passe = forms.CharField(max_length=100, widget=forms.PasswordInput(attrs={'placeholder': 'Mot de passe'}))   # widget = masquer le mot passe 
    confirmation_mot_de_passe = forms.CharField(max_length=100, widget=forms.PasswordInput(attrs={'placeholder': 'Confirmer le mot de passe'}))
