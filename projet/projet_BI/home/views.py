from django.shortcuts import render, redirect
from .forms import connexionForm
from .forms import souscriptionForm
from django.contrib.auth.hashers import make_password
from django.contrib.auth.hashers import check_password
from banque.models import Client, Compte
# re.match() -> va permettre de savoir si une chaine de caratère a un motif donné (expression régulière)
import re 


# Page d'acceuil #
def index(request) :
    return render(request, "index.html")


# Page de connexion #
def connexion(request) :
    # On initialise le message d'erreur à NONE
    message_erreur = None                                           

    if request.method == 'POST' :
        # On crée une instance du formulaire connexionForm via la requête POST
        form = connexionForm(request.POST)

        if form.is_valid() :
            identifiant = form.cleaned_data['identifiant']
            mot_de_passe = form.cleaned_data['mot_de_passe']

            try:
                client = Client.objects.get(id=identifiant)

                # On récupére le mot de passe hashé stocké du client dans la base de données 
                mot_de_passe_stocke = client.mdp

                # On vérifie si le mot de passe saisi correspond au mot de passe hashé stocké dans la base de données
                mot_de_passe_correspond = check_password(mot_de_passe, mot_de_passe_stocke)

                if mot_de_passe_correspond :
                    return redirect(f'http://127.0.0.1:8000/client/{identifiant}')
                
                else :
                    raise Client.DoesNotExist
            
            except ValueError:
                # Si on demande un entier on ne veut pas un caractère
                message_erreur = 'Identifiant non valide'          

            except Client.DoesNotExist:
                message_erreur = 'Identifiant ou mot de passe incorrect'
    else :
        # On crée une instance vide du formulaire connexionForm
        form = connexionForm()

    # 3eme argument -> dictionnaire contenant les informations que l'on souhaite afficher sur la vue
    return render(request, 'connexion.html', {'form': form, 'message_erreur': message_erreur})


# Page de souscription # 
def souscription(request) :
    # On initialise le message d'erreur à NONE
    message_erreur = None                                           

    if request.method == 'POST' :
        # On crée une instance du formulaire souscriptionForm via la requête POST
        form = souscriptionForm(request.POST)

        try:
            if form.is_valid() :
                nom = form.cleaned_data['nom']
                prenom = form.cleaned_data['prenom']
                mot_de_passe = form.cleaned_data['mot_de_passe']
                confirmation_mot_de_passe = form.cleaned_data['confirmation_mot_de_passe']

                # On ne veut pas de chiffre dans le nom et prenom
                if (not re.match("^[A-Za-zÀ-ÿ\- ']+$", nom)) or (not re.match("^[A-Za-zÀ-ÿ\- ']+$", prenom)) :
                    raise ValueError('Les champs nom et prénom ne peuvent contenir que des lettres.')

                # On vérifie les deux mots de passe
                if mot_de_passe != confirmation_mot_de_passe :
                    raise ValueError('Les mots de passe sont différents !')

                # On hashe le mot de passe
                mot_de_passe_hashe = make_password(mot_de_passe)
                nouveau_client = Client(nom_client=nom, prenom_client=prenom, mdp=mot_de_passe_hashe)
                nouveau_client.save()
                
                # On génére automatiquement les valeurs pour l'iban et le livret_A
                nouveau_client.generer_iban()
                nouveau_client.generer_livretA()
                
                nouveau_compte_IBAN = Compte(client=nouveau_client, type_compte='IBAN', numero =nouveau_client.iban ,solde='80')
                nouveau_compte_livret_A = Compte(client=nouveau_client, type_compte='Livret A', numero = nouveau_client.livret_A ,solde='0')

                nouveau_client.save()
                nouveau_compte_IBAN.save()
                nouveau_compte_livret_A.save()

                # On retient que l'on vient de la vue souscription (permet ensuite de savoir si on affiche l'id ou non)
                request.session['provenance'] = 'souscription'

                # Redirection après l'inscription réussie
                return redirect('http://127.0.0.1:8000/souscription/accesId/')  

        except ValueError as e:
            # On capturer l'erreur et on l'assigner au message d'erreur (str(objet) -> string)
            message_erreur = str(e)                                                             # e peut être un objet
            
    else :
        # On crée une instance vide du formulaire connexionForm
        form = souscriptionForm()

    # 3eme argument -> dictionnaire contenant les informations que l'on souhaite afficher sur la vue
    return render(request, 'souscription.html', {'form': form, 'message_erreur': message_erreur})    


# Page d'accès à l'id #
def accesId(request) :

    dernier_identifiant = None

    # On souhaite afficher l'id du dernier client créé seulement s'il vient d'être créé
    if request.session.get('provenance') == 'souscription':
    
        try:
            # On obtient le dernier client crée
            dernier_client = Client.objects.latest('id')

            # On récupère l'identifiant du dernier client
            dernier_identifiant = dernier_client.id

        except Client.DoesNotExist:
            # On gére l'exception si aucun client n'est trouvé
            dernier_identifiant = None

    if 'provenance' in request.session :
        # On efface la clé 'provenance' de la session (la session se ferme quand on ferme le navigateur)
        del request.session['provenance']
    
    return render(request, "accesId.html", {'dernier_identifiant': dernier_identifiant})
