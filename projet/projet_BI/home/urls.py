from django.urls import path, include
from . import views

# On relie les URLs aux fonctions de views.py #
urlpatterns = [
    path('', views.index, name='index'),
    path('connexion/', views.connexion, name='connexion'),
    path('souscription/', views.souscription, name='souscription'),
    path('souscription/accesId/', views.accesId, name='accesId'),
]
